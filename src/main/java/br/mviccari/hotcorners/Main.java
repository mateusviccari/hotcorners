package br.mviccari.hotcorners;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;
import org.jnativehook.mouse.NativeMouseListener;
import org.jnativehook.mouse.NativeMouseMotionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.*;
import java.time.Duration;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {

	private static final AtomicBoolean EXECUTANDO = new AtomicBoolean(false);
	private static final AtomicBoolean SAIU_DO_POINT_ZERO = new AtomicBoolean(true);
	private static final AtomicBoolean SAIU_DO_POINT_CENTRAL = new AtomicBoolean(true);
	private static final AtomicBoolean ESTA_COM_START_SCREEN_ABERTA = new AtomicBoolean(false);
	static final AtomicBoolean PAUSADO = new AtomicBoolean(true);
	private static final AtomicBoolean PAUSADO_POR_LISTA_DE_PROCESSOS = new AtomicBoolean(false);
	private static final NativeMouseMotionListener MOUSE_MOTION_LISTENER = createMouseMotionListener();
	private static final NativeKeyListener KEY_LISTENER = createKeyListener();
	private static final NativeMouseListener MOUSE_LISTENER = createMouseListener();
	private static final List<String> PROCESS_PAUSE_LIST = new ArrayList<>();

	public static void main(String[] args) {
		registrarAtalhoGlobal();
		iniciarMonitorDeListaDePausaDeProcessos();
	}

	static void togglePause() {
		Main.PAUSADO.set(!Main.PAUSADO.get());
		applyPause();
	}

	private static void applyPause() {
		if (Main.PAUSADO.get() || Main.PAUSADO_POR_LISTA_DE_PROCESSOS.get()) {
			removeListeners();
		} else {
			removeListeners();
			addListeners();
		}
	}

	private static void addListeners() {
		System.out.println("Adicionando listeners...");
		GlobalScreen.addNativeMouseMotionListener(MOUSE_MOTION_LISTENER);
		GlobalScreen.addNativeKeyListener(KEY_LISTENER);
		GlobalScreen.addNativeMouseListener(MOUSE_LISTENER);
	}

	private static void removeListeners() {
		System.out.println("Removendo listeners...");
		GlobalScreen.removeNativeMouseMotionListener(MOUSE_MOTION_LISTENER);
		GlobalScreen.removeNativeKeyListener(KEY_LISTENER);
		GlobalScreen.removeNativeMouseListener(MOUSE_LISTENER);
	}

	private static void registrarAtalhoGlobal(){
		try {
			TrayIcon.minimize();
			GlobalScreen.registerNativeHook();
			togglePause();

			LogManager.getLogManager().reset();
			Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
			logger.setLevel(Level.WARNING);
		} catch (NativeHookException e) {
			throw new RuntimeException(e);
		}
	}

	private static void iniciarMonitorDeListaDePausaDeProcessos() {
		File pauseList = new File("process_pause_list.txt");
		if (!pauseList.exists()) {
			return;
		}
		try {
			BufferedReader readerPauseList = new BufferedReader(new FileReader(pauseList));
			String linhaPauseList;
			while ((linhaPauseList = readerPauseList.readLine()) != null) {
				if (!linhaPauseList.trim().isEmpty()) {
					PROCESS_PAUSE_LIST.add(linhaPauseList);
				}
			}
			Thread threadMonitoramentoProcessos = new Thread(() -> {
				try {
					while (Thread.currentThread().isAlive()) {
                        Process process = Runtime.getRuntime().exec("tasklist.exe /fo csv /nh");
						BufferedReader readerProcesses = new BufferedReader(new InputStreamReader(process.getInputStream()));
						String linhaProcesso;
						Boolean detectouProcessoDaLista = false;
						while ((linhaProcesso = readerProcesses.readLine()) != null) {
							if (!linhaProcesso.trim().isEmpty()) {
								String nomeProcesso = linhaProcesso.split(",")[0].replace("'", "").replace("\"","");
								if (PROCESS_PAUSE_LIST.stream().filter(p -> p.equalsIgnoreCase(nomeProcesso)).count() > 0) {
									detectouProcessoDaLista = true;
									break;
								}
							}
						}
						if (detectouProcessoDaLista) {
							if (!PAUSADO_POR_LISTA_DE_PROCESSOS.get()) {
								PAUSADO_POR_LISTA_DE_PROCESSOS.set(true);
								applyPause();
							}
						} else {
							if (PAUSADO_POR_LISTA_DE_PROCESSOS.get()) {
								PAUSADO_POR_LISTA_DE_PROCESSOS.set(false);
								applyPause();
							}
						}
						Thread.sleep(Duration.ofSeconds(10).toMillis());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			threadMonitoramentoProcessos.setDaemon(true);
			threadMonitoramentoProcessos.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void executarTarefa(Runnable r) {
		Thread tarefa = new Thread(r);
		tarefa.setDaemon(true);
		tarefa.start();
	}

	private static NativeMouseMotionListener createMouseMotionListener() {
		return new NativeMouseMotionListener() {
			@Override
			public void nativeMouseMoved(NativeMouseEvent evt) {
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				double screenHeight = screenSize.getHeight();
				double screenWidth = screenSize.getWidth();

				if (evt.getX() <= 0 && evt.getY() <= 0) {
					executarTarefa(() -> {
						if (EXECUTANDO.get()) {
							return;
						}
						if (!SAIU_DO_POINT_ZERO.get()) {
							return;
						}
						EXECUTANDO.set(true);
						try {
							Point locationOriginal = MouseInfo.getPointerInfo().getLocation();
							Robot robot = new Robot();
							robot.mouseMove(3, 3);
							robot.mousePress(MouseEvent.BUTTON1_DOWN_MASK);
							robot.mouseRelease(MouseEvent.BUTTON1_DOWN_MASK);
							robot.mouseMove(locationOriginal.x, locationOriginal.y);
							try {
								Thread.sleep(300);
							} catch (InterruptedException ignored) {
							}
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							EXECUTANDO.set(false);
							SAIU_DO_POINT_ZERO.set(false);
						}
					});
				}
				if (evt.getX() > 5 && evt.getY() > 5) {
					SAIU_DO_POINT_ZERO.set(true);
				}

				double metadeDaMetadeTela = screenWidth / 4;
				if (evt.getX() >= metadeDaMetadeTela && evt.getX() <= screenWidth - metadeDaMetadeTela && evt.getY() <= 0) {
					executarTarefa(() -> {
						if (EXECUTANDO.get()) {
							return;
						}
						if (!SAIU_DO_POINT_CENTRAL.get()) {
							return;
						}
						EXECUTANDO.set(true);
						try {
							Point locationOriginal = MouseInfo.getPointerInfo().getLocation();
							Robot robot = new Robot();
                                                        robot.keyPress(KeyEvent.VK_WINDOWS);
                                                        robot.keyPress(KeyEvent.VK_TAB);
                                                        robot.keyRelease(KeyEvent.VK_TAB);
                                                        robot.keyRelease(KeyEvent.VK_WINDOWS);
							try {
								Thread.sleep(300);
							} catch (InterruptedException ignored) {
							}
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							EXECUTANDO.set(false);
							SAIU_DO_POINT_CENTRAL.set(false);
						}
					});
				}

				if (evt.getY() > 5 || evt.getX() <= 0) {
					SAIU_DO_POINT_CENTRAL.set(true);
				}

				if (evt.getX() <= 0 && evt.getY() >= screenHeight) {
					executarTarefa(() -> {
						if (EXECUTANDO.get()) {
							return;
						}
						EXECUTANDO.set(true);
						try {
							Robot robot = new Robot();
							robot.keyPress(KeyEvent.VK_CONTROL);
							robot.keyPress(KeyEvent.VK_WINDOWS);
							robot.keyPress(KeyEvent.VK_LEFT);
							robot.keyRelease(KeyEvent.VK_LEFT);
							robot.keyRelease(KeyEvent.VK_WINDOWS);
							robot.keyRelease(KeyEvent.VK_CONTROL);
							try {
								Thread.sleep(300);
							} catch (InterruptedException ignored) {
							}
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							EXECUTANDO.set(false);
						}
					});
				} else if (evt.getX() >= screenWidth && evt.getY() >= screenHeight) {
					executarTarefa(() -> {
						if (EXECUTANDO.get()) {
							return;
						}
						EXECUTANDO.set(true);
						try {
							Robot robot = new Robot();
							robot.keyPress(KeyEvent.VK_CONTROL);
							robot.keyPress(KeyEvent.VK_WINDOWS);
							robot.keyPress(KeyEvent.VK_RIGHT);
							robot.keyRelease(KeyEvent.VK_RIGHT);
							robot.keyRelease(KeyEvent.VK_WINDOWS);
							robot.keyRelease(KeyEvent.VK_CONTROL);
							try {
								Thread.sleep(300);
							} catch (InterruptedException ignored) {
							}
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							EXECUTANDO.set(false);
						}
					});
				}
			}

			@Override
			public void nativeMouseDragged(NativeMouseEvent evt) {
			}
		};
	}

	private static NativeKeyListener createKeyListener() {
		return new NativeKeyListener() {
			@Override public void nativeKeyPressed(NativeKeyEvent evt) {
				if (evt.getKeyCode() == NativeKeyEvent.VC_TAB && evt.getModifiers() == 4) {
					//WINDOWS + TAB
					ESTA_COM_START_SCREEN_ABERTA.set(!ESTA_COM_START_SCREEN_ABERTA.get());
				} else if (evt.getKeyCode() == NativeKeyEvent.VC_ESCAPE || evt.getKeyCode() == NativeKeyEvent.VC_SPACE
						|| evt.getKeyCode() == NativeKeyEvent.VC_ENTER || evt.getKeyCode() == NativeKeyEvent.VC_KP_ENTER) {
					ESTA_COM_START_SCREEN_ABERTA.set(false);
				}
			}

			@Override public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) { }

			@Override
			public void nativeKeyTyped(NativeKeyEvent evt) {
				if (ESTA_COM_START_SCREEN_ABERTA.get() && !(evt.getKeyChar() + "").trim().isEmpty()) {
					try {
						Robot robot = new Robot();
						robot.keyPress(KeyEvent.VK_WINDOWS);
						robot.keyRelease(KeyEvent.VK_WINDOWS);
						robot.mouseMove(345, 135);
						KeyStroke keyStroke = KeyStroke.getKeyStroke((evt.getKeyChar() + "").toUpperCase().charAt(0), 0);
						int keyCode = keyStroke.getKeyCode();
						Thread.sleep(80);
						robot.keyPress(keyCode);
						robot.keyRelease(keyCode);
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						ESTA_COM_START_SCREEN_ABERTA.set(false);
					}
				}
			}
		};
	}

	private static NativeMouseListener createMouseListener() {
		return new NativeMouseInputListener() {
			@Override
			public void nativeMouseClicked(NativeMouseEvent evt) {
				if (evt.getX() >= 36 && evt.getX() <= 71 && evt.getY() <= 25) {
					ESTA_COM_START_SCREEN_ABERTA.set(!ESTA_COM_START_SCREEN_ABERTA.get());
				} else {
					ESTA_COM_START_SCREEN_ABERTA.set(false);
				}
			}

			@Override
			public void nativeMousePressed(NativeMouseEvent evt) { }

			@Override
			public void nativeMouseReleased(NativeMouseEvent evt) { }

			@Override
			public void nativeMouseMoved(NativeMouseEvent evt) { }

			@Override
			public void nativeMouseDragged(NativeMouseEvent evt) { }
		};
	}

}
