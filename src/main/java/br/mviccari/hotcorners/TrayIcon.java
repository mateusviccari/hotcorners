package br.mviccari.hotcorners;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

class TrayIcon {

	static void minimize() {
		try {
			SystemTray tray = SystemTray.getSystemTray();
			PopupMenu popup = new PopupMenu();
			MenuItem menuSair = new MenuItem("Quit");
			menuSair.addActionListener(e -> System.exit(0));
			popup.add(menuSair);
			BufferedImage imageIconOn = ImageIO.read(TrayIcon.class.getResourceAsStream("/img/ic_on_16.png"));
			BufferedImage imageIconOff = ImageIO.read(TrayIcon.class.getResourceAsStream("/img/ic_off_16.png"));
			java.awt.TrayIcon trayIcon = new java.awt.TrayIcon(imageIconOn, "Hot Corners", popup);
			trayIcon.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getButton() == MouseEvent.BUTTON1) {
						Main.togglePause();
						trayIcon.setImage(Main.PAUSADO.get() ? imageIconOff : imageIconOn);
					}
				}
			});
			tray.add(trayIcon);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
